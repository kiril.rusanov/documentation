====================================
Ad Placements and Debugging Tools
====================================

This section covers other features that are available with placements. While not a requirement for the AdInMo SDK, we highly recommend you make use of these features.

Impression Validity
********************

To maximize your monetization it’s essential to understand what makes an impression valid. Here is a summary of what makes an impression valid:

* Time on screen: 1 second + 5 second cooldown - with cooldown being the minimum amount of time between cycling (ad switching)
* Angle must be less than or equal to 55 degrees, the image must also be facing the camera.
* Placement size must be at least 1.5% (coverage).
* Occlusion - this relates to blocking the placement e.g. putting a game object in front of placement blocking it from the camera. 

We recommend using the debugging tool to help optimise your placements to ensure they are generating valid impressions. For more information on the debugging tool see below.

Debugging Tools
****************
AdInMo’s debugging tools to help you optimise your placements.

Within Unity Editor go to the top menu, Adinmo, Manager.A new window will open with the debugging tools. Here, you can view your game key, placements and more.

Run your scene with this window open and information about your placements will be displayed, such as aspect ratio, impression total, and sample size. We recommend you enable ‘Debug Size Threshold’ when debugging as this displays your placement to help identify the sample size. 


Debug Size Threshold
======================

SDK 2.0 has a debugging tool that allows developers to ensure the size of their placements within the Unity Editor is large enough to generate revenue.

Select the Adinmo Manager in your scene and check ‘Debug Size Threshold’. 
If it is the correct size, a green bar will be displayed at the top of the placement and will countdown until the next ad appears. However, if the placement is not the correct size the bar will appear red.

.. image:: img/new-debug.jpg

SDK 1.5 and Lower
===================
In SDK 1.5 and below the debug tool will appear as a border around your placements. Green means you are able to generate revenue from your placement. Red means your placement is currently too small.


Occlusion
***********
The debugging tool will display a black and white sample image that highlights your placement, making it easy to see if other game objects in the scene are blocking your placement. This feature is only available in **SDK 2.0 and above**.

Occlusion Test Ignores Layers
==============================
Note this feature is only available in **Unity SDK 2.0.3 and above**.
This feature allows you to set specific layers to be ignored by the occlusion test. An example of where this might be used is when a placement has a collision box that may result in impressions being lost. The option can be found on the AdInMo texture component of a placement. This option can be found on the **adinmo texture component**.

.. image:: img/occlusion.jpg


Unsupported Local Axis Orientations
*************************************
In order to minimise the impact on your applications run speed, AdInMo’s SDK uses the local axis orientation to determine which direction the ad placement quad is facing, rather than the face normal, which would be slower. This means that if you use an unexpected local axis orientation for your placement quad, this could result in impressions counts not being calculated properly.

The orientation supported matches what you would get if you imported a standard unity quad into your scene, or one of our prefabs from the AdInMo/Prefabs/Meshes folder, where the local z axis (blue arrow  in unity) is in the opposite direction to the face normal.

**Unsupported**

.. image:: img/unsupported.jpg


**Supported**

.. image:: img/supported.jpg

If you use a standard unity plane object, or sometimes if you use imported geometry from a modelling package, this axis can be reversed, or orientated in an unexpected way. If you are using imported meshes, ensure that the local axis is set up correctly as in the above example. 


Placeholder Image
********************

On the AdinmoTexture component there is an option to disable the default placeholder. By default there is an AdInMo placeholder, this will display when no ads are being displayed. You can have your own placeholder image here by checking the disable placeholder option and adding your own to the texture.

.. image:: img/placeholder.jpg


Dynamic Ads
************

AdInMo supports the concept of dynamic ads, these are ad placements that are created programmatically rather than through the Unity editor. This allows AdInMo to support situations where a game developer programmatically creates objects in the game, our SDK supports the concept of then creating the ad unit programmatically once the object has been created.
Here’s a simple example of how this works using the AdInMo Unity SDK.

.. code-block:: csharp
  :linenos:

  using UnityEngine;
  public class InstantiationExample : MonoBehaviour
  {
    // Reference to the Prefab. Drag a Prefab with an AdInMo Texture component attached to it into this field in the Inspector.
      
    public GameObject myAdinmoPrefab;
    // This script will simply instantiate the Prefab when the game starts.
      
    void Start()
    {
        // Instantiate at position (0, 0, 0) and zero rotation.
        Instantiate(myPrefab, new Vector3(0, 0, 0), Quaternion.identity);
    }
  }


Placement Groups
***************************

Using groups allows you to have multiple placements displaying the same advertisement campaign. Currently, the AdInMo SDK supports up to 10 groups.

Once a group has been created, placements can be assigned to the group. The option to create/edit groups can be found either when creating a new placement or by editing an existing one.
